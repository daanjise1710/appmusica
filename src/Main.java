import java.io.*;
import java.sql.*;

public class Main {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;
    static String CONEXION = "jdbc:sqlserver://DESKTOP-TBB0MA9;DatabaseName=biblioteca_musical;user=sa;password=daanjise1710";

    public static void imprimirPlaylist() {

        try{
            out.println("Ingrese el nombre de la playlist de la que desea imprimir la informacion");
            String playlist = in.readLine();
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection conn = null;
            String query1 = "SELECT p.nombre, p.rating FROM playlist AS p WHERE p.nombre = '"+playlist+"'";
            String query2 = "   SELECT c.nombre " +
                    "           FROM cancion AS c " +
                    "           INNER JOIN canciones_playlist AS cp ON cp.id_cancion = c.id " +
                    "           INNER JOIN playlist as p ON cp.id_playlist = p.id" +
                    "           WHERE cp.id_playlist = (SELECT p.id FROM playlist AS p WHERE p.nombre = '"+playlist+"')";
            Statement stmt = null;
            ResultSet rs = null;
            conn =  DriverManager.getConnection(CONEXION);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query1);
            out.println("---IMPRIMIENDO LA INFORMACION DE LA PLAYLIST---");
            while(rs.next()){
                out.println("NOMBRE: " + rs.getString(1));
                out.println("CALIFICACION: " + rs.getString(2));
            }
            Statement stmt1 = null;
            ResultSet rs1 = null;
            stmt1 = conn.createStatement();
            rs1 = stmt1.executeQuery(query2);
            out.println("---CANCIONES---");
            while(rs1.next()){
                out.println(rs1.getString("nombre"));
            }
        }
        catch(ClassNotFoundException e){
            out.println(e.getMessage());
        }
        catch(SQLException e){
            out.println(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void imprimirCancion() {
        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection conn = null;
            String query = "SELECT * FROM cancion";
            Statement stmt = null;
            ResultSet rs = null;
            conn =  DriverManager.getConnection(CONEXION);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            out.println("---IMPRIMIENDO TODAS LAS CANCIONES---");
            out.println("---NOMBRE, RATING---");
            while(rs.next()){
                out.print("NOMBRE: " + rs.getString("nombre")+". ");
                out.println("RATING: " + rs.getString("rating"));
            }
        }
        catch(ClassNotFoundException e){
            out.println(e.getMessage());
        }
        catch(SQLException e){
            out.println(e.getMessage());
        }
    }

    public static void imprimirArtista() {
        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection conn = null;
            String query = "SELECT * FROM artista";
            Statement stmt = null;
            ResultSet rs = null;
            conn =  DriverManager.getConnection(CONEXION);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            out.println("---IMPRIMIENDO TODOS LOS GENEROS EXISTENTES---");
            out.println("---NOMBRE, DESCRIPCION---");
            while(rs.next()){
                out.print(rs.getString(1)+", ");
                out.println(rs.getString(2));
            }
        }
        catch(ClassNotFoundException e){
            out.println(e.getMessage());
        }
        catch(SQLException e){
            out.println(e.getMessage());
        }
    }

    public static void imprimirAlbum() {
        try{
            out.println("Ingrese el nombre del album que desea imprimir la informacion");
            String album = in.readLine();
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection conn = null;
            String query1 = "SELECT a.nombre, a.rating FROM playlist AS a WHERE a.nombre = '"+album+"'";
            String query2 = "   SELECT c.nombre " +
                    "           FROM cancion AS c " +
                    "           INNER JOIN canciones_playlist AS cp ON cp.id_cancion = c.id " +
                    "           INNER JOIN playlist as p ON cp.id_playlist = p.id" +
                    "           WHERE cp.id_playlist = (SELECT p.id FROM playlist AS p WHERE p.nombre = '"+album+"')";
            Statement stmt = null;
            ResultSet rs = null;
            conn =  DriverManager.getConnection(CONEXION);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query1);
            out.println("---IMPRIMIENDO LA INFORMACION DE LA PLAYLIST---");
            while(rs.next()){
                out.println("NOMBRE: " + rs.getString(1));
                out.println("CALIFICACION: " + rs.getString(2));
            }
            Statement stmt1 = null;
            ResultSet rs1 = null;
            stmt1 = conn.createStatement();
            rs1 = stmt1.executeQuery(query2);
            out.println("---CANCIONES---");
            while(rs1.next()){
                out.println(rs1.getString("nombre"));
            }
        }
        catch(ClassNotFoundException e){
            out.println(e.getMessage());
        }
        catch(SQLException e){
            out.println(e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void imprimirCompositor() {
        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection conn = null;
            String query = "SELECT * FROM compositor";
            Statement stmt = null;
            ResultSet rs = null;
            conn =  DriverManager.getConnection(CONEXION);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            out.println("---IMPRIMIENDO TODOS LOS GENEROS EXISTENTES---");
            out.println("---NOMBRE, DESCRIPCION---");
            while(rs.next()){
                out.print(rs.getString(1)+", ");
                out.println(rs.getString(2));
            }
        }
        catch(ClassNotFoundException e){
            out.println(e.getMessage());
        }
        catch(SQLException e){
            out.println(e.getMessage());
        }
    }

    public static void imprimirGenero() {
        try{
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            Connection conn = null;
            String query = "SELECT * FROM genero";
            Statement stmt = null;
            ResultSet rs = null;
            conn =  DriverManager.getConnection(CONEXION);
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            out.println("---IMPRIMIENDO TODOS LOS GENEROS EXISTENTES---");
            out.println("---NOMBRE, DESCRIPCION---");
            while(rs.next()){
                out.print(rs.getString(1)+", ");
                out.println(rs.getString(2));
            }
        }
        catch(ClassNotFoundException e){
            out.println(e.getMessage());
        }
        catch(SQLException e){
            out.println(e.getMessage());
        }
    }

    public static void main(String[] args) {
	    imprimirPlaylist();
        
    }
}
