package jimenez.darren.bl.entities;

import java.util.Date;

public class Album {
    private String name;
    private Date date;
    private double rating;

    public Album(String name, Date date, double rating) {
        this.name = name;
        this.date = date;
        this.rating = rating;
    }

    public Album() {
    }

    public String getName() {
        return name;
    }

    public Date getDate() {
        return date;
    }

    public double getRating() {
        return rating;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "Album{" +
                "name='" + name + '\'' +
                ", date=" + date +
                ", rating=" + rating +
                '}';
    }
}
