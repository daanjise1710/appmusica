package jimenez.darren.bl.entities;

import java.util.Objects;

public class Cancion {
    private String name;
    private String genre;
    private double rating;
    private int idArtista;

    public Cancion(String name, String genre, double rating, int idArtista) {
        this.name = name;
        this.genre = genre;
        this.rating = rating;
        this.idArtista = idArtista;
    }

    public Cancion() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public int getIdArtista() {
        return idArtista;
    }

    public void setIdArtista(int idArtista) {
        this.idArtista = idArtista;
    }

    @Override
    public String toString() {
        return "Cancion{" +
                "name='" + name + '\'' +
                ", genre='" + genre + '\'' +
                ", rating=" + rating +
                ", idArtista=" + idArtista +
                '}';
    }
}
