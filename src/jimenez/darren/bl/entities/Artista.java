package jimenez.darren.bl.entities;

import java.util.Date;

public class Artista {
    private String name;
    private String lastname;
    private String alias;
    private Date birthDate;
    private Date defunctionDate;
    private String birthPlace;
    private int age;
    private String reference;

    public Artista(String name, String lastname, String alias, Date birthDate, Date defunctionDate, String birthPlace, int age, String reference) {
        this.name = name;
        this.lastname = lastname;
        this.alias = alias;
        this.birthDate = birthDate;
        this.defunctionDate = defunctionDate;
        this.birthPlace = birthPlace;
        this.age = age;
        this.reference = reference;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Date getDefunctionDate() {
        return defunctionDate;
    }

    public void setDefunctionDate(Date defunctionDate) {
        this.defunctionDate = defunctionDate;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }


}
