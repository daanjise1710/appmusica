package jimenez.darren.bl.entities;

import java.util.Date;

public class Compositor {
    private String name;
    private String lastname;
    private Date birthDate;
    private String birthPlace;
    private int age;

    public Compositor(String name, String lastname, Date birthDate, String birthPlace, int age) {
        this.name = name;
        this.lastname = lastname;
        this.birthDate = birthDate;
        this.birthPlace = birthPlace;
        this.age = age;
    }

    public Compositor() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Compositor{" +
                "name='" + name + '\'' +
                ", lastname='" + lastname + '\'' +
                ", birthDate=" + birthDate +
                ", birthPlace='" + birthPlace + '\'' +
                ", age=" + age +
                '}';
    }
}
