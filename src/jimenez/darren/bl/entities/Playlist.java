package jimenez.darren.bl.entities;

import java.util.Date;

public class Playlist {
    private String name;
    private Date date;
    private double rating;

    public Playlist(String name, Date date, double rating) {
        this.name = name;
        this.date = date;
        this.rating = rating;
    }

    public Playlist() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "Playlist{" +
                "name='" + name + '\'' +
                ", date=" + date +
                ", rating=" + rating +
                '}';
    }
}
