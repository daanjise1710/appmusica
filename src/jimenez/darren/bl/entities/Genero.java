package jimenez.darren.bl.entities;

public class Genero {
    private String name;
    private String description;

    public Genero(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public Genero() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Genero{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
